
window.addEventListener('DOMContentLoaded', async () =>{
    const stateurl = "http://localhost:8000/api/states/"
    const stateResponce = await fetch(stateurl)
    if (stateResponce.ok){
        const data = await stateResponce.json()
        // console.log(data)
        let stateselect = document.querySelector("#state")
        for (let state of data.states){
            const option = document.createElement("option")
            option.selected = true
            option.innerHTML = state.name
            option.value = state.abbreviation
            stateselect.appendChild(option)
        }


    }

});
const form = document.getElementById("create-location-form")
form.addEventListener('submit', async (event) => {
    event.preventDefault();
    const object = {}
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {
        'Content-Type': 'application/json',
    },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
    form.reset();
    const newLocation = await response.json();
}

});
