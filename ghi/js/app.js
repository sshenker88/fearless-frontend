function createCard(name, description, pictureUrl, start, end, locationName) {
    return `
      <div class="card mb-4 ">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${start} - ${end}
      </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        out = document.querySelector(".body")
        out.innerHTML = ```<div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
      </div>```
      } else {
        const data = await response.json();
        let i = 0
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            let start = new Date(details.conference.starts).toLocaleDateString()
            let end = new Date(details.conference.ends).toLocaleDateString()
            const locationName = details.conference.location.name
            const html = createCard(title, description, pictureUrl, start, end, locationName);
            if (i % 3 === 0){
                const column = document.querySelector('.gg1stcol');
                column.innerHTML += html;
            }
            else if (i % 3 === 1){
                const column = document.querySelector('.gg2ndcol');
                column.innerHTML += html;
            }
            else {
                const column = document.querySelector('.gg3rdcol');
                column.innerHTML += html;
            }


            // const column = document.querySelector('.gg1stcol');
            // column.innerHTML += html;
          }
          i++
        }

      }
    } catch (e) {
        console.error(e)
      // Figure out what to do if an error is raised
    }

  });
