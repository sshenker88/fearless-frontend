
window.addEventListener('DOMContentLoaded', async () =>{
    const conferenceurl = "http://localhost:8000/api/conferences/"
    const conferenceResponce = await fetch(conferenceurl)
    if (conferenceResponce.ok){
        const data = await conferenceResponce.json()
        console.log(data)
        let conferenceSelect = document.querySelector("#conference")
        for (let location of data.conferences){
            const option = document.createElement("option")
            option.innerHTML = location.name
            const href = location.href
            option.value = href
            conferenceSelect.appendChild(option)
        }
        let loading = document.querySelector("#loading-conference-spinner")
        loading.classList.add("d-none")
        conferenceSelect.classList.remove("d-none")
    }

});
const form = document.getElementById("create-attendee-form")
form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(form);
    const object = Object.fromEntries(formData)
    const json = JSON.stringify(object);
    console.log(object)
    const attendeeUrl = object.conference;
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {
        'Content-Type': 'application/json',
    },
    };
    const response = await fetch("http://localhost:8001/api/attendees/", fetchConfig);
    if (response.ok) {
    form.reset();
    const attendeeResponce = await response.json();
    console.log(attendeeResponce)
    const hide_success = document.querySelector("#success-message")
    hide_success.innerHTML = `Congratulations, ${attendeeResponce.name}! You're all signed up!`
    hide_success.classList.remove("d-none")
}

});
