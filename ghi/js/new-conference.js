window.addEventListener('DOMContentLoaded', async () =>{
    const locationurl = "http://localhost:8000/api/locations/"
    const locationResponce = await fetch(locationurl)
    if (locationResponce.ok){
        const data = await locationResponce.json()
        console.log(data)
        let locationSelect = document.querySelector("#location")
        for (let location of data.locations){
            const option = document.createElement("option")
            option.innerHTML = location.name
            let href = location.href
            href = href.replace(/\D/g,'');
            option.value = Number(href)
            locationSelect.appendChild(option)
        }


    }

});
const form = document.getElementById("create-conference-form")
form.addEventListener('submit', async (event) => {
    event.preventDefault();
    const object = {}
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {
        'Content-Type': 'application/json',
    },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
    form.reset();
    const newConference = await response.json();
}

});
